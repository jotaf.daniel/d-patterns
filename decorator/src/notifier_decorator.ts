import { INotifier } from "./notifier_interface";

export class NotifierDecorator implements INotifier {
  protected decorated: INotifier;

  protected constructor(decorated: INotifier) {
    this.decorated = decorated;
  }

  send(msg: string): void {
    this.decorated.send(msg);
  }
}
