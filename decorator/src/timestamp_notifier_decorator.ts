import { NotifierDecorator } from "./notifier_decorator";
import { INotifier } from "./notifier_interface";

export class TimestampNotifierDecorator extends NotifierDecorator {
  public constructor(decorated: INotifier) {
    super(decorated);
  }

  send(msg: string): void {
    super.send(msg);
    console.log(new Date());
  }
}
