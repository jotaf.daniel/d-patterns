import { INotifier } from "./notifier_interface";
import { Notifier } from "./notifier";
import { TimestampNotifierDecorator } from "./timestamp_notifier_decorator";
import { AllCapsNotifierDecorator } from "./allcaps_notifier_decorator";

const environment: any = {
  debug: true,
  highlight: true,
};

function main() {
  let notifier: INotifier = new Notifier();

  if (environment.debug) {
    notifier = new TimestampNotifierDecorator(notifier);
  }

  if (environment.highlight) {
    notifier = new AllCapsNotifierDecorator(notifier);
  }

  notifier.send("hello, lari!");
}

main();
