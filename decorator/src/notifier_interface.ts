export interface INotifier {
  send: (msg: string) => void;
}
