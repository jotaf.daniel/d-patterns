import { NotifierDecorator } from "./notifier_decorator";
import { INotifier } from "./notifier_interface";

export class AllCapsNotifierDecorator extends NotifierDecorator {
  public constructor(decorated: INotifier) {
    super(decorated);
  }

  send(msg: string): void {
    super.send(msg.toLocaleUpperCase());
  }
}
