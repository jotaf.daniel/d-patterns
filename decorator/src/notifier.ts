import { INotifier } from "./notifier_interface";

export class Notifier implements INotifier {
  send(msg: string): void {
    console.log(msg);
  }
}
