import { Navigator } from "./Router";
import { BikeRouter } from "./bike_router";
import { CarRouter } from "./car_router";

const environment: any = {
  vehicle: "bike",
};

function main() {
  const gps: Navigator = new Navigator();

  if (environment.vehicle == "bike") {
    gps.setStrategy(new BikeRouter());
  } else {
    gps.setStrategy(new CarRouter());
  }

  const route: string[] = gps.calculateRoute("Popp's Barbershop");

  route.forEach((path: string): void => {
    console.log(path);
  });
}

main();
