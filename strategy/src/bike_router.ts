import { IRouterCalculator } from "./router_calculator_interface";

export class BikeRouter implements IRouterCalculator {
  route_between(start: string, end: string): string[] {
    return [
      `begin at ${start}`,
      `ride on St. Peter's Avenue for 15m heading north`,
      `turn left on Flower's Garden Park - entrance A`,
      `follow Duck's Lane for 350m`,
      `turn left on Creek Lane`,
      `ride for 400m`,
      `turn right on Basketball Courts then turn left on Food Trucks Street`,
      `exit Flower's Garden Park - entrance D`,
      `turn right on Palm Street`,
      `ride for 15m`,
      `${end} is on your left`,
    ];
  }
}
