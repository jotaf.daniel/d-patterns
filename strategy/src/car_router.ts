import { IRouterCalculator } from "./router_calculator_interface";

export class CarRouter implements IRouterCalculator {
  route_between(start: string, end: string): string[] {
    return [
      `begin at [${start}]`,
      `stay on St. Peter's Avenue for 500m`,
      `turn left on 4th Avenue and stay on the right lane`,
      `drive for 120m then turn right on Palm Street`,
      `drive for 15m`,
      `${end} is on your left`,
    ];
  }
}
