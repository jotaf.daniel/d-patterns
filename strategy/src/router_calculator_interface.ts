export interface IRouterCalculator {
  route_between: (start: string, end: string) => string[];
}
