import { IRouterCalculator } from "./router_calculator_interface";
import { CarRouter } from "./car_router";

export class Navigator {
  private routerStrategy: IRouterCalculator;

  constructor() {
    this.routerStrategy = new CarRouter();
  }

  setStrategy(strategy: IRouterCalculator): void {
    this.routerStrategy = strategy;
  }

  calculateRoute(to: string): string[] {
    const from = this.currentLocation;
    return this.routerStrategy.route_between(from, to);
  }

  private get currentLocation(): string {
    return "Home";
  }
}
